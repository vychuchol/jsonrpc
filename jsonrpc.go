package jsonrpc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// Error represents JSON-RPC error.
type Error int

const (
	// Version represents JSON-RPC specification version.
	Version = "2.0"

	// OK represents successed value for Error.
	OK = Error(0)

	// ErrorParse represents error, when invalid JSON was received by the server.
	// An error occurred on the server while parsing the JSON text.
	ErrorParse = Error(-32700)

	// ErrorInvalidRequest represents error, when the JSON sent is not a valid Request object.
	ErrorInvalidRequest = Error(-32600)

	// ErrorMethodNotFound represents error, when the method does not exist / is not available.
	ErrorMethodNotFound = Error(-32601)

	// ErrorInvalidParams represents error, when invalid method parameter(s) are received.
	ErrorInvalidParams = Error(-32602)

	// ErrorInternal represents internal error.
	ErrorInternal = Error(-32603)

	// ErrorServer is reserved for implementation-defined server-errors.
	// This is minimum server error number.
	ErrorServer = Error(-32000)

	// ErrorServerMax is maximum server error number.
	ErrorServerMax = Error(-32099)
)

// ErrorMessages represents text messages for all errors.
var ErrorMessages = map[Error]string{
	ErrorParse:          "Parse error",
	ErrorInvalidRequest: "Invalid Request",
	ErrorMethodNotFound: "Method not found",
	ErrorInvalidParams:  "Invalid params",
	ErrorInternal:       "Internal error",
	ErrorServer:         "Server error",
}

func (e Error) String() string {
	if ErrorMessages != nil {
		if message, ok := ErrorMessages[e]; ok {
			return message
		}
		// specific for server errors
		if e > ErrorServer && e <= ErrorServerMax {
			if message, ok := ErrorMessages[ErrorServer]; ok {
				return message
			}
		}
	}
	return strconv.Itoa(int(e))
}

func GetInt64(json map[string]interface{}, key string) (val int64, ok bool) {
	if json == nil {
		return 0, false
	}
	if v, vOk := json[key]; vOk {
		switch vt := v.(type) {
		case float64:
			return int64(vt), true
		case string:
			i, err := strconv.ParseInt(vt, 10, 64)
			return i, err == nil
		default:
		}
	}
	return 0, false
}

// Method represents one method in JSON-RPC.
type Method func(method string, params map[string]interface{}, result map[string]interface{}) (e Error, message string)

// Before represents preprocessor for each JSON-RPC call.
type Before func(req *http.Request, method string, params map[string]interface{}, result map[string]interface{})

// Collection represents one JSON-RPC method collection.
type Collection struct {
	Before  Before
	Methods map[string]Method
}

func writeJSON(rw http.ResponseWriter, data map[string]interface{}) error {
	rw.Header().Set("Content-Type", "application/json")
	output, err := json.Marshal(data)
	if err != nil {
		return err
	}
	rw.Write(output)
	return nil
}

func writeError(rw http.ResponseWriter, id interface{}, e Error, data interface{}) error {
	d := data
	if err, ok := d.(error); ok {
		d = fmt.Sprintf("%v", err)
	}
	return writeJSON(rw, map[string]interface{}{
		"jsonrpc": Version,
		"id":      id,
		"error": map[string]interface{}{
			"code":    e,
			"message": e.String(),
			"data":    d,
		},
	})
}

// Handler can be used for HTTP.
func (c *Collection) Handler(rw http.ResponseWriter, req *http.Request) {
	// TODO Only for POST?
	data := make(map[string]interface{})

	buffer := bytes.Buffer{}
	buffer.ReadFrom(req.Body)
	if err := json.Unmarshal(buffer.Bytes(), &data); err != nil {
		err = fmt.Errorf("can not parse JSON-RPC call: %v", err)
		log.Println(err)
		writeError(rw, nil, ErrorParse, err)
		return
	}

	jsonrpc, ok := data["jsonrpc"].(string)
	if !ok {
		err := fmt.Errorf("missing JSON-RPC version")
		log.Println(err)
		writeError(rw, nil, ErrorInvalidRequest, err)
		return
	}
	if jsonrpc != Version {
		err := fmt.Errorf("invalid JSON-RPC version: %s", jsonrpc)
		log.Println(err)
		writeError(rw, nil, ErrorInvalidRequest, err)
		return
	}

	// TODO Property ID can be number too.
	id, ok := data["id"].(string)
	if !ok {
		err := fmt.Errorf("missing JSON-RPC id")
		log.Println(err)
		writeError(rw, nil, ErrorInvalidRequest, err)
		return
	}

	method, ok := data["method"].(string)
	if !ok {
		err := fmt.Errorf("missing JSON-RPC method")
		log.Println(err)
		writeError(rw, id, ErrorInvalidRequest, err)
		return
	}

	params, _ := data["params"].(map[string]interface{})

	if c.Methods == nil {
		err := fmt.Errorf("not found JSON-RPC method: %s", method)
		log.Println(err)
		writeError(rw, id, ErrorMethodNotFound, err)
		return
	}
	m, ok := c.Methods[method]
	if !ok {
		err := fmt.Errorf("not found JSON-RPC method: %s", method)
		log.Println(err)
		writeError(rw, id, ErrorMethodNotFound, err)
		return
	}

	result := make(map[string]interface{})
	if c.Before != nil {
		c.Before(req, method, params, result)
	}
	e, message := m(method, params, result)
	if e != OK {
		err := fmt.Errorf("invalid JSON-RPC call: %s", message)
		log.Println(err)
		writeError(rw, id, e, err)
		return
	}

	writeJSON(rw, map[string]interface{}{
		"jsonrpc": Version,
		"id":      id,
		"result":  result,
	})
}
